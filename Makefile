install:
	yarn

run: install
	yarn dev

deploy: install
	yarn generate
	ln -s ../../releases .output/public/.

test:
	node --max-old-space-size=8000 tests.mjs
