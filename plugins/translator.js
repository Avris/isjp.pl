import { defineNuxtPlugin } from "#app";
import pl from '../translations/pl.js';
import en from '../translations/en.js';

const translations = {pl, en}

export default defineNuxtPlugin((nuxtApp) => {
    const url = process.server
        ? nuxtApp.ssrContext?.event.req.url?.replace(/\?.*/, '')
        : window.location.pathname;
    const locale = url === '/en' ? 'en' : 'pl';

    nuxtApp.vueApp.use({
        install(app, options) {
            app.config.globalProperties.$locale = locale;
            app.config.globalProperties.$t = (key, params = {}) => {
                let value = translations[locale];
                for (let part of key.split('.')) {
                    value = value[part];
                    if (value === undefined) {
                        console.error('Cannot find translation: ' + key);
                        return undefined;
                    }
                }

                for (let k in params) {
                    if (params.hasOwnProperty(k)) {
                        value = Array.isArray(value)
                            ? value.map(v => v.replace(new RegExp('%' + k + '%', 'g'), params[k]))
                            : value.replace(new RegExp('%' + k + '%', 'g'), params[k]);
                    }
                }

                return value;
            }
        }
    });
});
