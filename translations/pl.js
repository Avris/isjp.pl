export default {
    title: 'Inkluzywny Słownik Języka Polskiego',
    description: 'Słownik do sprawdzania pisowni obejmujący formy niebinarne i neutralne płciowo.',
    keywords: 'spellcheck, sprawdzanie pisowni, pisownia, słownik, polski, sjp, inkluzywny, niebinarny, neutralny płciowo',
    intro: [
        'W języku tak silnie zgenderyzowanym jak polski osoby niebinarne spotykają się z niemałym problemem: ' +
        'czy mogą jakoś wyrazić się bez konieczności wyboru między rodzajem męskim a żeńskim? ' +
        '{https://zaimki.pl/kolektyw-rjn=Kolektyw „Rada Języka Neutralnego”} na stronie {https://zaimki.pl=zaimki.pl} ' +
        'zbiera i promuje propozycje form języka niebinarnego oraz neutralnego płciowo.',

        'Niestety, nawet propozycje takie jak {https://zaimki.pl/ono=rodzaj neutralny} – ' +
        'które choć są rzadkie w mainstreamie, to jednak ze względu na aprobatę ' +
        '{https://rjp.pan.pl/index.php?option=com_content&view=article&id=317:byom-byo&catid=44&Itemid=208=Rady Języka Polskiego} ' +
        'czy {https://ksiazki.wp.pl/w-tvp-wysmiewali-osoby-niebinarne-profesor-bralczyk-rozumie-dlaczego-6616979703601760a=prof. Bralczyka}, ' +
        'oraz występowanie {https://zaimki.pl/korpus#Alexander%20Adamowicz=gramatykach z XVIII} i {https://zaimki.pl/korpus#Maksymilian%20Jakubowicz=i XIX wieku} ' +
        'czy u {https://zaimki.pl/korpus#Liryki%20loza%C5%84skie=Mickiewicza} i {https://zaimki.pl/korpus#Lem%20Maska=Lema} ' +
        'nie powinny wzbudzać kontrowersji – ' +
        'i tak {https://twitter.com/neutratywy/status/1458110112292298759=spotykają się z queerfobicznym oporem} osób decyzyjnych.',

        'Dlatego wyszłośmy z własną inicjatywą, by <strong>słowniki do sprawdzania pisowni rozszerzyć o brakujące formy</strong>. ' +
        'Bazując na {https://sjp.pl/slownik/ort/=słowniku od sjp.pl}, na zasadach licencji {https://creativecommons.org/licenses/by/4.0/=CC BY 4.0}, ' +
        'tworzymy dwie wersje słownika w formacie dla {https://hunspell.github.io/=Hunspell}/{https://en.wikipedia.org/wiki/MySpell=MySpell}: ' +
        'normatywną oraz neologiczną.',
    ],
    links: {
        petition: {
            label: 'Nasza petycja',
            subtitle: 'w sprawie słowników do sprawdzania pisowni',
        },
        pronounspage: {
            label: 'zaimki.pl',
            subtitle: 'więcej informacji o języku niebinarnym i neutralnym płciowo',
        },
    },
    beta: [
        'Nasz słownik jest <strong>w fazie eksperymentalnej, w wersji αlpha</strong>. ' +
        'Oznacza to, że możesz spodziewać się nowych wersji dość często, ' +
        'oraz oczywiście że możesz napotkać w nim błędy. ' +
        'Prosimy o zgłaszanie ich na adres {mailto:isjp@zaimki.pl=isjp@zaimki.pl} ' +
        'albo {https://gitlab.com/PronounsPage/ISJP=wysyłanie merge requestów}.',

        'Tworzymy ISJP wolontariacko, własnymi siłami, bez doświadczenia w podobnych projektach i w mocno ograniczonym wolnym czasie – ' +
        'więc <strong>prosimy o cierpliwość i wyrozumiałość</strong>. 😅',
    ],
    abandoned: [
        'Niestety, musimy <strong>porzucić projekt</strong>… ' +
        'Choć wciąż wierzymy w ideę i konieczność stworzenia bardziej inkluzywnego słownika autokorekty, ' +
        'musimy przyznać, że by ją zrealizować, brakuje nam mocy przerobowych oraz siły przebicia – ' +
        'choćby i idealny słownik na nic się zda, jeśli wielkie firmy nie są nim zainteresowane, więc w każdym programie trzeba instalować go ręcznie. ' +
        'Jeśli jesteś w stanie pomóc ożywić projekt, prosimy o kontakt mailowy pod adresem {mailto:isjp@zaimki.pl=isjp@zaimki.pl} ' +
        'albo {https://gitlab.com/PronounsPage/ISJP=wysyłanie merge requestów}.',
    ],
    dictionaries: {
        normatywny: {
            label: 'Słownik normatywny',
            description: 'Jest to słownik rozszerzający bazę sjp.pl o formy, których w niej brakuje, mimo że są normatywne – ' +
                'i to nawet jeśli {https://rjp.pan.pl/index.php?option=com_content&view=article&id=317:byom-byo&catid=44&Itemid=208=są wprost aprobowane} ' +
                'przez osoby językoznawcze jako zgodne z systemem językowym.',
        },
        neologiczny: {
            label: 'Słownik neologiczny',
            description: 'Słownik ten zawiera oprócz zestawu normatywnego również neologiczne formy stworzone dla lepszej reprezentacji ' +
                '{https://zaimki.pl/pytania#niebinarnosc=osób niebinarnych} ' +
                'oraz dla przeciwdziałania {https://pl.wikipedia.org/wiki/M%C4%99ska_dominacja_j%C4%99zykowa=męskiej dominacji językowej}.',
        },
    },
    features: {
        neuterSingular: 'Formy rodzaju neutralnego',
        feminine: 'Feminatywy',
        terminology: 'Terminologia queerowa',
        grammarNames: 'Nazwy nowych form gramatycznych',

        neuterPlural: 'Formy mnogie rodzaju neutralnego',
        neuterNouns: 'Neutratywy',
        postgender: 'Dukaizmy',
        postgenderNouns: 'Dukatywy',
        placeholderX: 'Formy z iksem',
        placeholderXNouns: 'Iksatywy',
        nonpersonal: 'Zaimki nie tylko osobowe',

        extend: {
            normative: 'To, co w normatywnym, oraz'
        },
    },
    download: {
        action: 'Pobierz',
        unavailable: 'Prace trwają, zajrzyj później…',
        version: 'Wersja',
        examples: 'Na przykład',
        removedExamples: 'Usunięte na przykład',
        wip: 'W budowie',
        info: 'Więcej info',
    },
    instructions: {
        header: 'Jak użyć naszego słownika w programach?',
        beta: 'Ta sekcja dopiero powstaje. Jeśli chcesz nam pomóc ją tworzyć, przetestuj nasz słownik w danym programie ' +
            'i podeślij nam instrukcję dodawania go na adres {mailto:isjp@zaimki.pl=isjp@zaimki.pl} ' +
            'lub {https://gitlab.com/PronounsPage/isjp.pl=zgłoś merge requesta}.',
        intro: [
            'ISJP jest dostępny w popularnym formacie {https://hunspell.github.io/=Hunspell}/{https://en.wikipedia.org/wiki/MySpell=MySpell}, ' +
            'co oznacza, że powinien być kompatybilny z wieloma systemami operacyjnymi i aplikacjami. ' +
            'Składa się z dwóch głównych plików: ' +
            '<code>.dic</code>, zawierającego słowa, oraz <code>.aff</code>, zawierającego afiksy i reguły ich używania. ' +
            'W wielu przypadkach skopiowanie tych dwóch plików do odpowiedniego folderu powinno wystarczyć, by zacząć używać nowego słownika.',
        ],
        apps: {
            MacOS: 'Wklej rozpakowane pliki słownika do <code>~/Library/Spelling</code>, ' +
                'a następnie wejdź w <code>System Preferences</code> » <code>Keyboard</code> » <code>Text</code> ' +
                'i w polu <code>Spelling</code> wybierz <code>Polski (Library)</code>.',
            JetBrains: '{https://www.jetbrains.com/help/mps/settings-spelling.html=Instrukcja}',
        }
    },
    footer: {
        authors: 'Stworzone przez {https://zaimki.pl/@andrea=Andreę Vos} z {https://zaimki.pl/kolektyw-rjn=kolektywu „Rada Języka Neutralnego”}',
        contact: 'Kontakt: {mailto:isjp@zaimki.pl=isjp@zaimki.pl}, {https://kolektiva.social/@neutratywy=@neutratywy}',
        license: '{https://gitlab.com/PronounsPage/isjp.pl=Strona} i {https://gitlab.com/PronounsPage/ISJP=generator słownika} ' +
            'udostępnione na licencji {https://oql.avris.it/license?c=Andrea%20Vos%7Chttps://avris.it=OQL 1.0}, ' +
            'sam słownik udostępniony na licencji {https://creativecommons.org/licenses/by/4.0/=CC BY 4.0}',
        tools: 'Użyte narzędzia: {https://www.python.org/=Python}, {https://v3.nuxtjs.org/=NuxtJS}, ' +
            '{https://v3.vuejs.org/=VueJS}, {https://fonts.google.com/noto/specimen/Noto+Emoji=Noto Emoji}, {https://www.gradientmagic.com=GradientMagic}, ' +
            '{http://bonanova.wtf/=font BonaNova}, {https://fontawesome.com/v5.15=FontAwesome}',
    },
};
