export default {
    title: 'Inclusive Dictionary of the Polish Language',
    description: 'Spellchecker dictionary extended with nonbinary and gender-neutral words.',
    keywords: 'spellcheck, spell, dictionary, polish, nonbinary gender-neutral',
    intro: [
        'Polish is a very gendered language, one can\'t even say “I went” or “I\'m hungry” without specifying one\'s gender. ' +
        'That proves to be a big problem for nonbinary folks who can\'t easily express themselves ' +
        'without a choice between masculine and feminine forms being forced upon them. ',
        '{https://zaimki.pl/kolektyw-rjn=Our collective „Neutral Language Council”} creates a website {https://zaimki.pl=zaimki.pl} ' +
        'where we collect and promote ideas for making our language more inclusive towards enbies.',

        'Unfortunately, even proposals such as applying {https://zaimki.pl/ono=neutrum} to people – ' +
        'which, albeit rare in mainstream, is considered grammatically correct by the ' +
        '{https://rjp.pan.pl/index.php?option=com_content&view=article&id=317:byom-byo&catid=44&Itemid=208=Polish Language Council} ' +
        'and suggested by linguistic experts like {https://ksiazki.wp.pl/w-tvp-wysmiewali-osoby-niebinarne-profesor-bralczyk-rozumie-dlaczego-6616979703601760a=prof. Bralczyk} for use by enbies, ' +
        'appear in {https://zaimki.pl/korpus#Alexander%20Adamowicz=grammar books from 18th} and {https://zaimki.pl/korpus#Maksymilian%20Jakubowicz=19th century} ' +
        'and has been used by authors like {https://zaimki.pl/korpus#Liryki%20loza%C5%84skie=Mickiewicz} or {https://zaimki.pl/korpus#Lem%20Maska=Lem} ' +
        'so it shouldn\'t really be controversial – ' +
        'yet it still {https://twitter.com/neutratywy/status/1458110112292298759=encounters queerfobic resistance} from decision-makers.',

        'That\'s why we started working on <strong>extending spellcheck dictionaries with the missing forms</strong>. ' +
        'Based on {https://sjp.pl/slownik/ort/=sjp.pl\'s dictionary}, under the {https://creativecommons.org/licenses/by/4.0/=CC BY 4.0} license, ' +
        'we create two versions of the dictionary in the {https://hunspell.github.io/=Hunspell}/{https://en.wikipedia.org/wiki/MySpell=MySpell} format: ' +
        'normative and neological.',
    ],
    links: {
        petition: {
            label: 'Our petition',
            subtitle: 'regarding updating spellcheck dictionaries',
        },
        pronounspage: {
            label: 'zaimki.pl',
            subtitle: 'more information about nonbinary and gender-neutral language',
        },
    },
    beta: [
        'Our dictionary is <strong>in an experimental phase, in αlpha version</strong>. ' +
        'That means you can expect frequent updates ' +
        'and of course encounter bugs. ' +
        'Please report them to {mailto:isjp@zaimki.pl=isjp@zaimki.pl} ' +
        'or {https://gitlab.com/PronounsPage/ISJP=create a merge request}.',

        'We create ISJP as volunteers, in our free time, without any previous experience in similar projects – ' +
        'so <strong>we ask you to be please be patient and understanding</strong>. 😅',
    ],
    abandoned: [
        'Unfortunately, we need to <strong>abandon the project</strong>… ' +
        'As much as we still belive in the idea and necessity of creating a more inclusive Polish autocorrect dictionary, ' +
        'we must admint that in order to achieve that goal we\'re lacking resources and power – ' +
        'even a perfect dictionary is useless, if big tech companies are not interested, so one must install it manually in every single program. ' +
        'If you can help revive the project, please contact us at {mailto:isjp@zaimki.pl=isjp@zaimki.pl} ' +
        'or {https://gitlab.com/PronounsPage/ISJP=create a merge request}.',
    ],
    dictionaries: {
        normatywny: {
            label: 'Normative dictionary',
            description: 'This dictionary extends sjp.pl\'s base with forms that it\'s lacking even though they\'re normative ' +
                'and even {https://rjp.pan.pl/index.php?option=com_content&view=article&id=317:byom-byo&catid=44&Itemid=208=explicitly approved} ' +
                'by linguists as compatible with the language system.',
        },
        neologiczny: {
            label: 'Neological dictionary',
            description: 'On top of the normative set, this dictionary also includes neological forms created for better representation ' +
                'of {https://zaimki.pl/pytania#niebinarnosc=nonbinary people} ' +
                'and to counteract {https://en.wikipedia.org/wiki/Androcentrism=androcentrism in language}.',
        },
    },
    features: {
        neuterSingular: 'Neuter forms',
        feminine: 'Feminine nouns',
        terminology: 'Queer terminology',
        grammarNames: 'Names of new grammar forms',

        neuterPlural: 'Neuter plural forms',
        neuterNouns: 'Neuter nouns',
        postgender: 'Postgender forms',
        postgenderNouns: 'Postgender nouns',
        placeholderX: 'Forms with X as a placeholder',
        placeholderXNouns: 'Nouns with X as a placeholder',
        nonpersonal: 'Non-personal pronouns',

        extend: {
            normative: 'Normative plus'
        },
    },
    download: {
        action: 'Download',
        unavailable: 'Work in progress, please come back later…',
        version: 'Version',
        examples: 'For example',
        removedExamples: 'Removed for example',
        wip: 'Work in progress',
        info: 'More info',
    },
    instructions: {
        header: 'How to use our dictionary in apps?',
        beta: 'This section is under construction. If you want to help build it, please test our dictionary in the given app ' +
            'and send us the instructions to {mailto:isjp@zaimki.pl=isjp@zaimki.pl} ' +
            'or {https://gitlab.com/PronounsPage/isjp.pl=create a merge request}.',
        intro: [
            'ISJP is available in a popular format: {https://hunspell.github.io/=Hunspell}/{https://en.wikipedia.org/wiki/MySpell=MySpell}, ' +
            'which means it should be compatible with many operating systems and apps. ' +
            'It consists of two main files: ' +
            '<code>.dic</code> containing words and <code>.aff</code> containing affixes and their rules. ' +
            'In most cases it should suffice to copy those files to a relecant folder.',
        ],
        apps: {
            MacOS: 'Copy the unzipped dictionary files to <code>~/Library/Spelling</code>, ' +
                'and then open <code>System Preferences</code> » <code>Keyboard</code> » <code>Text</code> ' +
                'and in the <code>Spelling</code> dropdown select <code>Polski (Library)</code>.',
            JetBrains: '{https://www.jetbrains.com/help/mps/settings-spelling.html=Manual}',
        }
    },
    footer: {
        authors: 'Created by {https://zaimki.pl/@andrea=Andrea Vos} from {https://zaimki.pl/kolektyw-rjn=the collective „Neutral Language Council”}',
        contact: 'Contact: {mailto:isjp@zaimki.pl=isjp@zaimki.pl}, {https://kolektiva.social/@neutratywy=@neutratywy}',
        license: '{https://gitlab.com/PronounsPage/isjp.pl=This website} and {https://gitlab.com/PronounsPage/ISJP=the generator of the dictionary} ' +
            'are pubLished under {https://oql.avris.it/license?c=Andrea%20Vos%7Chttps://avris.it=OQL 1.0}, ' +
            'the dictionary itself under {https://creativecommons.org/licenses/by/4.0/=CC BY 4.0}',
        tools: 'Tools used: {https://www.python.org/=Python}, {https://v3.nuxtjs.org/=NuxtJS}, ' +
            '{https://v3.vuejs.org/=VueJS}, {https://fonts.google.com/noto/specimen/Noto+Emoji=Noto Emoji}, {https://www.gradientmagic.com=GradientMagic}, ' +
            '{http://bonanova.wtf/=font BonaNova}, {https://fontawesome.com/v5.15=FontAwesome}',
    },
};
