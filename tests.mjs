import cliui from 'cliui';
import chalk from 'chalk';
import fs from 'fs';
import Spellchecker from 'hunspell-spellchecker';
import path from 'path';

const dir = `${path.resolve(path.dirname(''))}/releases`;
const manifest = JSON.parse(fs.readFileSync(`${dir}/manifest.json`).toString());

const loadDictionary = (dirname) => {
    console.log(`Parsing ${dirname}`);
    const spellchecker = new Spellchecker();

    spellchecker.use(spellchecker.parse({
        dic: fs.readFileSync(`${dir}/${dirname}/pl_PL.dic`),
        aff: fs.readFileSync(`${dir}/${dirname}/pl_PL.aff`),
    }));

    memory();

    return {
        spellchecker,
        dirname,
        issues: 0,
    };
}

const memory = () => {
    const mu = process.memoryUsage();
    const gbNow = mu.heapUsed / 1024 / 1024 / 1024;  // # bytes / KB / MB / GB
    const gbRounded = Math.round(gbNow * 100) / 100;

    console.log(`Heap allocated ${gbRounded} GB`);
}

const ui = cliui();

const dictionaries = {
    base: loadDictionary('base'),
    normatywny: loadDictionary(manifest.normatywny.filename),
    neologiczny: loadDictionary(manifest.neologiczny.filename),
}

const words = {};
for (let [name, dictionary] of Object.entries(dictionaries)) {
    const testFile = `${dir}/${dictionary.dirname}/tests_pl_PL.json`;
    if (!fs.existsSync(testFile)) { continue; }

    const tests = JSON.parse(fs.readFileSync(testFile));
    for (let word of tests.addedExamples) {
        if (!words.hasOwnProperty(word)) {
            words[word] = {};
        }
        words[word][name] = true;
    }
    for (let word of tests.removedExamples) {
        if (!words.hasOwnProperty(word)) {
            words[word] = {};
        }
        words[word][name] = false;
    }
}


let row = [{
    text: 'Word',
    width: 30,
}];
for (let [name, dictionary] of Object.entries(dictionaries)) {
    row.push({
        text: name,
        width: 20,
    })
}
ui.div(...row)

for (let [word, expectedIn] of Object.entries(words)) {
    let row = [{
        text: word,
        width: 30,
    }];
    for (let [name, dictionary] of Object.entries(dictionaries)) {
        const has = dictionary.spellchecker.check(word)
        const expected = expectedIn[name];
        const wrong = expected !== undefined && has !== expected;
        let colour = chalk.white;
        if (expected !== undefined) {
            colour = wrong ? chalk.red : chalk.green;
        }
        if (wrong) {
            dictionary.issues++;
        }

        row.push({
            text: colour(has),
            width: 30,
        })
    }
    ui.div(...row);
}

let issues = 0;
row = [{
    text: 'Issues',
    width: 30,
}];
for (let [name, dictionary] of Object.entries(dictionaries)) {
    row.push({
        text: dictionary.issues ? chalk.red(dictionary.issues) : chalk.green(dictionary.issues),
        width: 30,
    })
    issues += dictionary.issues;
}
ui.div(...row)

console.log(ui.toString())

console.log(issues > 0
    ? chalk.red(`${issues} issues found!`)
    : chalk.green('All good!')
)
